# Building Microservice for Technical Test BTPN

## Introduction
* Microservice for technical test backend


## Requirements
* Node, NPM, Redis & MongoDB

## Installation
* Clone this repo:
* Install dependecies: ``` npm install ```

## Running apps
* cd to repo
* node server.js
* go to http://localhost:3000/

