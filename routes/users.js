const express = require('express');
const router = express.Router();
const userController = require('../app/api/controllers/users');

router.get('/', userController.getAll);
router.get('/:userId', userController.getById);
router.put('/:userId', userController.updateById);
router.delete('/:userId', userController.deleteById);

router.post('/create-user', userController.create);
router.post('/authenticate', userController.authenticate);
router.get('/getByUserIdentity/:identityId', userController.getByIdentityNumber);
router.get('/getByUserAccount/:accountId', userController.getByAccountNumber);

module.exports = router;