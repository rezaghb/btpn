const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

//Define a schema
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	name: {
		type: String,
		trim: true,		
		required: true,
	},
	email: {
		type: String,
		trim: true,
		unique : true, required : true, dropDups: true 
	},
	password: {
		type: String,
		trim: true,
		required: true
	},
	userName: {
		type: String,
		trim: true,
		unique : true, required : true, dropDups: true 
	},
	accountNumber: {
		type: String,
		trim: true,
		unique : true, required : true, dropDups: true 
	},
	identityNumber: {
		type: String,
		trim: true,
		unique : true, required : true, dropDups: true 
	}
});

UserSchema.pre('save', function(next){
this.password = bcrypt.hashSync(this.password, saltRounds);
next();
});

module.exports = mongoose.model('User', UserSchema);