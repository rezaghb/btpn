const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const VehicleSchema = new Schema({
	plate_number: {
		type: String,
		trim: true,		
		unique : true, required : true, dropDups: true
	},
	color: {
		type: String,
		trim: true,
		required: true
	},
	brand: {
		type: String,
		trim: true,
		required: true
	},
	vehicle_type: {
		type: String,
		trim: true,
		required: true
	},
	user_id: {
		type: String,
		trim: true,
		required: true
	}
});

module.exports = mongoose.model('Vehicle', VehicleSchema)