
const vehicleModel = require('../models/vehicles');					

module.exports = {
	getById: function(req, res, next) {
		console.log(req.body);
		vehicleModel.findById(req.params.vehicleId, function(err, vehicleInfo){
			if (err) {
				next(err);
			} else {
				res.json({status:"success", message: "Vehicle found!!!", data:{vehicles: vehicleInfo}});
			}
		});
	},

	getAll: function(req, res, next) {
		let vehiclesList = [];

		vehicleModel.find({}, function(err, vehicles){
			if (err){
				next(err);
			} else{
				for (let vehicle of vehicles) {
					vehiclesList.push({id: vehicle._id, brand: vehicle.brand, color: vehicle.color, plateNumber:vehicle.plate_number, type:vehicle.vehicle_type});
				}
				res.json({status:"success", message: "Vehicles list found!!!", data:{vehicles: vehiclesList}});
							
			}

		});
	},

	updateById: function(req, res, next) {
		vehicleModel.findByIdAndUpdate(req.params.vehicleId,{brand: req.body.brand, color: req.body.color, plate_number:req.body.plate_number, vehicle_type:req.body.vehicle_type}, function(err, vehicleInfo){

			if(err)
				next(err);
			else {
				res.json({status:"success", message: " updated successfully!!!", data:null});
			}
		});
	},

	deleteById: function(req, res, next) {
		vehicleModel.findByIdAndRemove(req.params.vehicleId, function(err, vehicleInfo){
			if(err)
				next(err);
			else {
				res.json({status:"success", message: " deleted successfully!!!", data:null});
			}
		});
	},

	create: function(req, res, next) {
		vehicleModel.create({  brand: req.body.brand, color: req.body.color, plate_number:req.body.plate_number, vehicle_type:req.body.vehicle_type, user_id:req.body.user_id }, function (err, result) {
				  if (err) 
				  	next(err);
				  else
				  	res.json({status: "success", message: " added successfully!!!", data: null});
				  
				});
	},

}					