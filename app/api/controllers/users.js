const userModel = require('../models/users');
const bcrypt = require('bcrypt');	
const jwt = require('jsonwebtoken');				

module.exports = {
	getById: function(req, res, next) {
		console.log(req.body);
		userModel.findById(req.params.userId, function(err, userInfo){
			if (err) {
				next(err);
			} else {
				res.json({status:"success", message: "data found!!!", data:{users: userInfo}});
			}
		});
	},

	getAll: function(req, res, next) {
		let userList = [];

		userModel.find({}, function(err, users){
			if (err){
				next(err);
			} else{
				for (let user of users) {
					userList.push({ name: user.name, email: user.email,  userName:user.userName, accountNumber:user.accountNumber, identityNumber:user.identityNumber });
				}
				res.json({status:"success", message: "data list found!!!", data:{users: userList}});
							
			}

		});
	},

	updateById: function(req, res, next) {
		userModel.findByIdAndUpdate(req.params.userId,{ name: req.body.name, email: req.body.email, password: req.body.password, userName:req.body.userName, accountNumber:req.body.accountNumber, identityNumber:req.body.identityNumber }, function(err, vehicleInfo){

			if(err)
				next(err);
			else {
				res.json({status:"success", message: " updated successfully!!!", data:null});
			}
		});
	},

	deleteById: function(req, res, next) {
		userModel.findByIdAndRemove(req.params.userId, function(err, userInfo){
			if(err)
				next(err);
			else {
				res.json({status:"success", message: " deleted successfully!!!", data:null});
			}
		});
	},




	getByAccountNumber : function(req, res, next) {
		console.log(req.body);
		userModel.findOne({accountNumber:req.params.accountId}, function(err, userInfo){
			if (err) {
				next(err);
			} else {
				res.json({status:"success", message: "data found!!!", data:{users: userInfo}});
			}
		});
	},
	getByIdentityNumber : function(req, res, next) {
		console.log(req.body);
		userModel.findOne({identityNumber:req.params.identityId}, function(err, userInfo){
			if (err) {
				next(err);
				res.json({status:"error", message: err});
			} else {
				res.json({status:"success", message: "data found!!!", data:{users: userInfo}});
			}
		});
	},


	create: function(req, res, next) {
		
		userModel.create({ name: req.body.name, email: req.body.email, password: req.body.password, userName:req.body.userName, accountNumber:req.body.accountNumber, identityNumber:req.body.identityNumber }, function (err, result) {
				  if (err) 
				  	next(err);
				  else
				  	res.json({status: "success", message: "data added successfully!!!", data: null});
				  
				});
	},

	authenticate: function(req, res, next) {
		userModel.findOne({email:req.body.email}, function(err, userInfo){
					if (err) {
						next(err);
					} else {

						if(userInfo != null && bcrypt.compareSync(req.body.password, userInfo.password)) {

						 const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), { expiresIn: '1h' }); 

						 res.json({status:"success", message: "user found!!!", data:{user: userInfo, token:token}});	

						}else{

							res.json({status:"error", message: "Invalid email/password!!!", data:null});

						}
					}
				});
	},

}					
